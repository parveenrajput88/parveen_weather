package api;

import android.os.AsyncTask;

import java.lang.ref.WeakReference;

/**
 * Created by Parveen on 24-03-2017.
 */

public class ApiClient {

    public static void getWeatherData(String apiUrl, ApiResponseHandler responseHandler,int apiId){
        RetrieveFeedTask task = new RetrieveFeedTask(new WeakReference<ApiResponseHandler>(responseHandler), apiUrl,apiId);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
