package api;

/**
 * Created by Parveen on 24-03-2017.
 */

public class ApiUrls {

    public static String getLocationWeatherUrl(double lat, double lng){
        return "http://api.openweathermap.org/data/2.5/forecast/daily?lat="+lat+"&lon="+lng+"&cnt=14";
    }

    public static String getCityWeatherUrl(String cityName){
        return "http://api.openweathermap.org/data/2.5/forecast/daily?q="+cityName+"&cnt=14";
    }
}
