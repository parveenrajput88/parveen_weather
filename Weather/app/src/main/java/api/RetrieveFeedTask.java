package api;

import android.os.AsyncTask;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

import beans.WeatherResponse;
import utils.AppConstant;

/**
 * Created by Parveen on 24-03-2017.
 */

public class RetrieveFeedTask  extends AsyncTask<Void, Void, Object> {

    private WeakReference<ApiResponseHandler> mResponseHandler;
    private String mUrl;
    private final int  mApiId;
    RetrieveFeedTask(WeakReference<ApiResponseHandler> responseHandler, String url, int apiId){
        mResponseHandler=responseHandler;
        mUrl=url;
        mApiId=apiId;
    }

    protected void onPreExecute() {
    }

    protected Object doInBackground(Void... voids) {
        HttpURLConnection urlConnection=null;
        try {
            URL url = new URL(mUrl +  "&APPID=" + AppConstant.WEATHER_API_KEY);
            urlConnection= (HttpURLConnection) url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line).append("\n");
            }
            bufferedReader.close();

            if(null!=stringBuilder.toString()){
                return        parseData(stringBuilder.toString());
            }

        }
        catch(Exception e) {
            e.printStackTrace();
            return null;
        }finally{
            if(null!=urlConnection){
                urlConnection.disconnect();
            }
        }
        return null;
    }

    protected void onPostExecute(Object response) {
        if(mResponseHandler!=null && mResponseHandler.get()!=null){
            if(response == null) {
                mResponseHandler.get().onFailure();
                return;
            }
            mResponseHandler.get().onSuccess(response);

        }
    }

    private Object parseData(String jsonResponse){
        WeatherResponse weatherResponse=null;
        switch (mApiId){
            case AppConstant.LOCATION_WEATHER_API:
                 weatherResponse = new Gson().fromJson(jsonResponse, WeatherResponse.class);

                return weatherResponse;

            case AppConstant.CITY_WEATHER_API:
                 weatherResponse = new Gson().fromJson(jsonResponse, WeatherResponse.class);
                return weatherResponse;
        }
        return null;
    }
}
