package api;

/**
 * Created by Parveen on 24-03-2017.
 */

public interface ApiResponseHandler<T> {
    public void onSuccess(T t);
    public void onFailure();
}
