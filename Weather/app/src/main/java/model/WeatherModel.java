package model;

import android.util.Log;

import api.ApiClient;
import api.ApiResponseHandler;
import api.ApiUrls;
import beans.WeatherResponse;
import utils.AppConstant;
import interfaces.Operations;

/**
 * Created by Parveen on 24-03-2017.
 */

public class WeatherModel implements Operations.PresenterModelOps {


    private Operations.ModelPresenterOps mModelPresenterOps;
    public WeatherModel(Operations.ModelPresenterOps modelPresenterOps){
        mModelPresenterOps=modelPresenterOps;
    }
    @Override
    public void getCurrentCityData(double lat, double lng) {
        String weatherUrl = ApiUrls.getLocationWeatherUrl(lat,lng);
        ApiClient.getWeatherData(weatherUrl, new ApiResponseHandler<WeatherResponse>() {

            @Override
            public void onSuccess(WeatherResponse weatherResponse) {
                Log.i("Airtel", "weatherResponse " + weatherResponse);

                if(weatherResponse!=null && mModelPresenterOps!=null){
                   mModelPresenterOps.onWeatherDataFetch(weatherResponse);
                }
            }

            @Override
            public void onFailure() {

            }
        }, AppConstant.LOCATION_WEATHER_API);
    }

    @Override
    public void getCityData(String cityName) {
        String weatherUrl = ApiUrls.getCityWeatherUrl(cityName);
        ApiClient.getWeatherData(weatherUrl, new ApiResponseHandler<WeatherResponse>() {

            @Override
            public void onSuccess(WeatherResponse weatherResponse) {
                Log.i("Airtel", "weatherResponse " + weatherResponse);
                if(weatherResponse!=null && mModelPresenterOps!=null ){
                    mModelPresenterOps.onWeatherDataFetch(weatherResponse);
                }

            }

            @Override
            public void onFailure() {

            }
        }, AppConstant.CITY_WEATHER_API);
    }

    @Override
    public void onDestroy() {
        mModelPresenterOps=null;
    }


}
