package airtel.com.weather.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import beans.List;
import utils.AppConstant;
import airtel.com.weather.R;

/**
 * Created by Parveen on 24-03-2017.
 */

public class WeatherDetailActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_detail_activity);

        List list=getIntent().getParcelableExtra(AppConstant.EXTRA_WEATHER_DATA);

        setData(list);
    }

    private void setData(List list){

        TextView humidity = (TextView) findViewById(R.id.e_humidity);
        TextView pressure = (TextView) findViewById(R.id.e_pressure);
        TextView tempMax = (TextView) findViewById(R.id.e_max_temperature);
        TextView tempMin = (TextView) findViewById(R.id.e_min_temperature);
        TextView snow = (TextView) findViewById(R.id.e_snow);
        TextView speed = (TextView) findViewById(R.id.e_speed);

        humidity.setText("Humidity: " + list.getHumidity());
        pressure.setText("Pressure: " + list.getPressure());
        tempMax.setText("Max Temp: " + list.getTemp().getMax());
        tempMin.setText("Min Temp: " + list.getTemp().getMin());
        snow.setText("Snow: " + list.getSnow());
        speed.setText("Speed: " + list.getSpeed());
    }
}
