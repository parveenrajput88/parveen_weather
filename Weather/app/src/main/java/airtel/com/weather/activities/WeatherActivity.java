package airtel.com.weather.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

import beans.List;
import beans.WeatherResponse;
import model.WeatherModel;
import presenter.WeatherPresenter;
import utils.AppConstant;
import adapters.WeatherAdapter;
import airtel.com.weather.R;
import interfaces.Operations;
import location.LocationHandler;

public class WeatherActivity extends AppCompatActivity implements View.OnClickListener, Operations.ViewOps {

    private ArrayList<List> weatherList;
    private WeatherAdapter weatherAdapter;
    private View mProgressLayout;
    private Operations.PresentationOps mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        Button button = (Button) findViewById(R.id.e_current_city_data);
        button.setOnClickListener(this);

        WeatherPresenter presenter =new WeatherPresenter(this);
        WeatherModel weatherModel=new WeatherModel(presenter);
        presenter.setPresenterModelOps(weatherModel);

        mPresenter=presenter;

        SearchView searchView = (SearchView) findViewById(R.id.mySearchView);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.i("Airtel", "query " + query);
                weatherList.clear();
                mPresenter.getCityData(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        init();
    }

    private void init() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.e_weather_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        weatherList=new ArrayList<>();
        weatherAdapter=new WeatherAdapter(this, weatherList, new WeatherAdapter.onItemClickListner() {
            @Override
            public void onItemClick(int pos) {
                if(mPresenter!=null){
                    mPresenter.onWeatherSelect(pos);
                }
            }
        });
        recyclerView.setAdapter(weatherAdapter);
        mProgressLayout=findViewById(R.id.e_progress_layout);
    }


    @Override
    public void onClick(View view) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                    AppConstant.REQUEST_GET_LOCATIONS);

            return;
        }
        mPresenter.getCurrentCityData();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case AppConstant.REQUEST_GET_LOCATIONS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    mPresenter.getCurrentCityData();
                }
                return;
            }
        }
    }


    @Override
    public void fetchLocation() {
        LocationHandler locationHandler = new LocationHandler(this);
        Location location = locationHandler.getLocation();

        if(location==null){
            Toast.makeText(this, "Location not accessible, Try Again", Toast.LENGTH_SHORT).show();
            return;
        }
        weatherList.clear();
        mPresenter.onLocationFetch(location.getLatitude(),location.getLongitude());
    }

    @Override
    public void onWeatherDataFetch(WeatherResponse responses) {
        weatherList.clear();
        weatherList.addAll(responses.getList());
        weatherAdapter.notifyDataSetChanged();
    }

    @Override
    public void showLoader() {
        mProgressLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        mProgressLayout.setVisibility(View.GONE);
    }

    @Override
    public void showWeatherDetail(int pos) {
        Intent intent=new Intent(this, WeatherDetailActivity.class);
        intent.putExtra(AppConstant.EXTRA_WEATHER_DATA,weatherList.get(pos));
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }
}
