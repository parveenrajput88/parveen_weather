package beans;

/**
 * Created by Parveen on 24-03-2017.
 */

public class City {

    private String lon;

    private String geoname_id;

    private String name;

    private String iso2;

    private String type;

    private String lat;

    private String population;

    private String country;

    public String getLon ()
    {
        return lon;
    }

    public void setLon (String lon)
    {
        this.lon = lon;
    }

    public String getGeoname_id ()
    {
        return geoname_id;
    }

    public void setGeoname_id (String geoname_id)
    {
        this.geoname_id = geoname_id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getIso2 ()
    {
        return iso2;
    }

    public void setIso2 (String iso2)
    {
        this.iso2 = iso2;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getLat ()
    {
        return lat;
    }

    public void setLat (String lat)
    {
        this.lat = lat;
    }

    public String getPopulation ()
    {
        return population;
    }

    public void setPopulation (String population)
    {
        this.population = population;
    }

    public String getCountry ()
    {
        return country;
    }

    public void setCountry (String country)
    {
        this.country = country;
    }
}
