package beans;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Parveen on 24-03-2017.
 */

public class Temp implements Parcelable{
    private String min;

    private String eve;

    private String max;

    private String morn;

    private String night;

    private String day;

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(min);
        dest.writeString(eve);
        dest.writeString(max);
        dest.writeString(morn);
        dest.writeString(night);
        dest.writeString(day);
    }

    private Temp(Parcel in){
        min = in.readString();
        eve = in.readString();
        max= in.readString();
        morn = in.readString();
        night = in.readString();
        day = in.readString();
    }


    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Temp createFromParcel(Parcel in) {
            return new Temp(in);
        }

        public Temp[] newArray(int size) {
            return new Temp[size];
        }
    };
    @Override
    public int describeContents() {
        return 0;
    }


    public String getMin ()
    {
        return min;
    }

    public void setMin (String min)
    {
        this.min = min;
    }

    public String getEve ()
    {
        return eve;
    }

    public void setEve (String eve)
    {
        this.eve = eve;
    }

    public String getMax ()
    {
        return max;
    }

    public void setMax (String max)
    {
        this.max = max;
    }

    public String getMorn ()
    {
        return morn;
    }

    public void setMorn (String morn)
    {
        this.morn = morn;
    }

    public String getNight ()
    {
        return night;
    }

    public void setNight (String night)
    {
        this.night = night;
    }

    public String getDay ()
    {
        return day;
    }

    public void setDay (String day)
    {
        this.day = day;
    }

}
