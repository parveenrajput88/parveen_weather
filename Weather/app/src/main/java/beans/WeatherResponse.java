package beans;

import java.util.ArrayList;

/**
 * Created by Parveen on 24-03-2017.
 */

public class WeatherResponse {
    private String message;

    private String cnt;

    private String cod;

    private ArrayList<List> list;

    private City city;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getCnt ()
    {
        return cnt;
    }

    public void setCnt (String cnt)
    {
        this.cnt = cnt;
    }

    public String getCod ()
    {
        return cod;
    }

    public void setCod (String cod)
    {
        this.cod = cod;
    }

    public ArrayList<List> getList ()
    {
        return list;
    }

    public void setList (ArrayList<List> list)
    {
        this.list = list;
    }

    public City getCity ()
    {
        return city;
    }

    public void setCity (City city)
    {
        this.city = city;
    }

}
