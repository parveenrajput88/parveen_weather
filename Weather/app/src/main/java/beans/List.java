package beans;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Parveen on 24-03-2017.
 */

public class List implements Parcelable{
    private String clouds;

    private String dt;

    private String humidity;

    private String pressure;

    private String speed;

    private String snow;

    private String deg;

    private ArrayList<Weather> weather;

    private Temp temp;

    @Override
    public int describeContents() {
        return 0;
    }
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public List createFromParcel(Parcel in) {
            return new List(in);
        }

        public List[] newArray(int size) {
            return new List[size];
        }
    };
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(clouds);
        dest.writeString(dt);
        dest.writeString(humidity);
        dest.writeString(pressure);
        dest.writeString(speed);
        dest.writeString(snow);
        dest.writeString(deg);
        if(temp!=null){
            dest.writeParcelable(temp,flags);
        }
    }

    private List(Parcel in){
        clouds = in.readString();
        dt = in.readString();
        humidity= in.readString();
        pressure = in.readString();
        speed = in.readString();
        snow = in.readString();
        deg = in.readString();
        temp=in.readParcelable(Temp.class.getClassLoader());

    }

    public String getClouds ()
    {
        return clouds;
    }

    public void setClouds (String clouds)
    {
        this.clouds = clouds;
    }

    public String getDt ()
    {
        return dt;
    }

    public void setDt (String dt)
    {
        this.dt = dt;
    }

    public String getHumidity ()
    {
        return humidity;
    }

    public void setHumidity (String humidity)
    {
        this.humidity = humidity;
    }

    public String getPressure ()
    {
        return pressure;
    }

    public void setPressure (String pressure)
    {
        this.pressure = pressure;
    }

    public String getSpeed ()
    {
        return speed;
    }

    public void setSpeed (String speed)
    {
        this.speed = speed;
    }

    public String getSnow ()
    {
        return snow;
    }

    public void setSnow (String snow)
    {
        this.snow = snow;
    }

    public String getDeg ()
    {
        return deg;
    }

    public void setDeg (String deg)
    {
        this.deg = deg;
    }

    public ArrayList<Weather> getWeather ()
    {
        return weather;
    }

    public void setWeather (ArrayList<Weather> weather)
    {
        this.weather = weather;
    }

    public Temp getTemp ()
    {
        return temp;
    }

    public void setTemp (Temp temp)
    {
        this.temp = temp;
    }
}
