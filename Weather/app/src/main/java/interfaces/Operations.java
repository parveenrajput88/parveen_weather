package interfaces;

import beans.WeatherResponse;

/**
 * Created by Parveen on 24-03-2017.
 */

public interface Operations {

    public static interface PresentationOps{
        public void getCurrentCityData();
        public void onLocationFetch(double lat, double lng);
        public void getCityData(String cityName);
        public void onDestroy();
        public void onWeatherSelect(int pos);
    }

    public static interface ViewOps{
//        public void showToast(String msg);
        public void fetchLocation();
        public void onWeatherDataFetch(WeatherResponse responses);
        public void showLoader();
        public void hideLoader();
        public void showWeatherDetail(int pos);

    }

    public static interface PresenterModelOps{
        public void getCurrentCityData(double lat, double lng);
        public void getCityData(String cityName);
        public void onDestroy();
    }

    public static interface ModelPresenterOps{
        public void onWeatherDataFetch(WeatherResponse responses);
        public void onApiFail();

    }
}
