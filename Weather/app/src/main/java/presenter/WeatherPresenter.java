package presenter;

import beans.WeatherResponse;
import interfaces.Operations;

/**
 * Created by Parveen on 24-03-2017.
 */

public class WeatherPresenter implements Operations.PresentationOps, Operations.ModelPresenterOps {

    private Operations.ViewOps mViewOps;
    private Operations.PresenterModelOps mPresenterModelOps;
    public WeatherPresenter(Operations.ViewOps viewOps){
        mViewOps=viewOps;

    }

    public void setPresenterModelOps( Operations.PresenterModelOps presenterModelOps){
        mPresenterModelOps=presenterModelOps;
    }
    @Override
    public void getCurrentCityData() {
        if (mViewOps!=null){
            mViewOps.fetchLocation();
        }
    }

    @Override
    public void onLocationFetch(double lat, double lng) {
        if (mViewOps!=null){
            mViewOps.showLoader();
        }
        mPresenterModelOps.getCurrentCityData(lat,lng);
    }

    @Override
    public void onWeatherDataFetch(WeatherResponse responses) {
        if (mViewOps!=null){
            mViewOps.hideLoader();
            mViewOps.onWeatherDataFetch(responses);
        }
    }

    @Override
    public void onApiFail() {
        if (mViewOps!=null){
            mViewOps.hideLoader();
        }
    }


    @Override
    public void getCityData(String cityName) {
        if (mViewOps!=null){
            mViewOps.showLoader();
        }
        mPresenterModelOps.getCityData(cityName);
    }

    @Override
    public void onDestroy() {
        mViewOps=null;
    }

    @Override
    public void onWeatherSelect(int pos) {
        if(mViewOps!=null){
            mViewOps.showWeatherDetail(pos);
        }
    }
}
