package adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;

import beans.List;
import airtel.com.weather.R;

import static airtel.com.weather.R.string.humidity;

/**
 * Created by Parveen on 24-03-2017.
 */

public class WeatherAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<List> weatherList;
    private LayoutInflater inflater;

    public static interface onItemClickListner{
        public void onItemClick(int pos);
    };

    private onItemClickListner clickListner;
    public WeatherAdapter(Context context,ArrayList<List> list, onItemClickListner clickListner){
        this.weatherList=list;
        inflater=LayoutInflater.from(context);
        this.clickListner=clickListner;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new WeatherViewHolder( inflater.inflate(R.layout.weather_item,null,false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((WeatherViewHolder)holder).bindData(weatherList.get(position),position);
    }

    @Override
    public int getItemCount() {
        return weatherList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    private class WeatherViewHolder extends RecyclerView.ViewHolder{

        private TextView humidity, pressure, tempMax, tempMin, snow, speed;
        public WeatherViewHolder(View view){
            super(view);
            humidity = (TextView) view.findViewById(R.id.e_humidity);
            pressure = (TextView) view.findViewById(R.id.e_pressure);
            tempMax = (TextView) view.findViewById(R.id.e_max_temperature);
            tempMin = (TextView) view.findViewById(R.id.e_min_temperature);
            snow = (TextView) view.findViewById(R.id.e_snow);
            speed = (TextView) view.findViewById(R.id.e_speed);
        }

        public void bindData(List  list, final int pos){
            humidity.setText("Humidity: " + list.getHumidity());
            pressure.setText("Pressure: " + list.getPressure());
            tempMax.setText("Max Temprature: " + list.getTemp().getMax() + "K");
            tempMin.setText("Min Temprature: " + list.getTemp().getMin() + "K");
            snow.setText("Snow: " + list.getSnow());
            speed.setText("Speed: " + list.getSpeed());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(null!=clickListner){
                        clickListner.onItemClick(pos);
                    }
                }
            });
        }
    }
}
